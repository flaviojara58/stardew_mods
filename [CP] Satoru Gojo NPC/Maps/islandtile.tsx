<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="islandtile" tilewidth="16" tileheight="16" tilecount="2080" columns="32">
 <image source="islandtile.png" width="512" height="1040"/>
 <tile id="0">
  <animation>
   <frame tileid="299" duration="250"/>
   <frame tileid="299" duration="250"/>
   <frame tileid="299" duration="250"/>
   <frame tileid="294" duration="250"/>
   <frame tileid="295" duration="250"/>
   <frame tileid="296" duration="250"/>
   <frame tileid="296" duration="250"/>
   <frame tileid="295" duration="250"/>
   <frame tileid="294" duration="250"/>
   <frame tileid="293" duration="250"/>
  </animation>
 </tile>
 <tile id="261">
  <animation>
   <frame tileid="261" duration="250"/>
   <frame tileid="261" duration="250"/>
   <frame tileid="261" duration="250"/>
   <frame tileid="261" duration="250"/>
   <frame tileid="262" duration="250"/>
   <frame tileid="263" duration="250"/>
   <frame tileid="264" duration="250"/>
   <frame tileid="264" duration="250"/>
   <frame tileid="265" duration="250"/>
   <frame tileid="266" duration="250"/>
   <frame tileid="267" duration="250"/>
   <frame tileid="261" duration="250"/>
   <frame tileid="261" duration="250"/>
  </animation>
 </tile>
 <tile id="293">
  <animation>
   <frame tileid="293" duration="250"/>
   <frame tileid="293" duration="250"/>
   <frame tileid="293" duration="250"/>
   <frame tileid="293" duration="250"/>
   <frame tileid="294" duration="250"/>
   <frame tileid="295" duration="250"/>
   <frame tileid="296" duration="250"/>
   <frame tileid="296" duration="250"/>
   <frame tileid="297" duration="250"/>
   <frame tileid="298" duration="250"/>
   <frame tileid="299" duration="250"/>
   <frame tileid="293" duration="250"/>
   <frame tileid="293" duration="250"/>
  </animation>
 </tile>
 <tile id="421">
  <animation>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="422" duration="250"/>
   <frame tileid="423" duration="250"/>
   <frame tileid="424" duration="250"/>
   <frame tileid="424" duration="250"/>
   <frame tileid="425" duration="250"/>
   <frame tileid="426" duration="250"/>
   <frame tileid="427" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
  </animation>
 </tile>
 <tile id="453">
  <animation>
   <frame tileid="453" duration="250"/>
   <frame tileid="453" duration="250"/>
   <frame tileid="453" duration="250"/>
   <frame tileid="453" duration="250"/>
   <frame tileid="454" duration="250"/>
   <frame tileid="455" duration="250"/>
   <frame tileid="456" duration="250"/>
   <frame tileid="456" duration="250"/>
   <frame tileid="457" duration="250"/>
   <frame tileid="458" duration="250"/>
   <frame tileid="459" duration="250"/>
   <frame tileid="453" duration="250"/>
   <frame tileid="453" duration="250"/>
  </animation>
 </tile>
 <tile id="694">
  <animation>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="422" duration="250"/>
   <frame tileid="423" duration="250"/>
   <frame tileid="424" duration="250"/>
   <frame tileid="424" duration="250"/>
   <frame tileid="425" duration="250"/>
   <frame tileid="426" duration="250"/>
   <frame tileid="427" duration="250"/>
   <frame tileid="421" duration="250"/>
   <frame tileid="421" duration="250"/>
  </animation>
 </tile>
 <tile id="1286">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="1318">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
</tileset>
