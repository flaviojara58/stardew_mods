<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.10" tiledversion="1.10.2" name="picnicdate2" tilewidth="16" tileheight="16" tilecount="2304" columns="32">
 <image source="picnicdate2.png" width="512" height="1152"/>
 <tile id="322">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="323">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="327">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="1051">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="1052">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
 <tile id="1053">
  <properties>
   <property name="Passable" value="T"/>
  </properties>
 </tile>
</tileset>
